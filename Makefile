# Release checklist:
# - git tag vX.Y
# - git push --tags origin master
# - Write release notes on gitlab: https://gitlab.com/latexpand/latexpand/tags
# - make
# - Upload latexpand.zip to https://www.ctan.org/upload
#   . email = git@matthieu-moy.fr
#   . Suggested CTAN directory = /support/latexpand
all: dist/latexpand.zip

dist/latexpand/README: latexpand
	pod2text latexpand > $@

GIT_VERSION = ${shell git describe --tags HEAD}

.PHONY: all force
dist/latexpand.zip: force
	-$(RM) -r dist/
	mkdir -p dist/latexpand
	@$(MAKE) dist/latexpand/README
	@echo "latexpand version $(GIT_VERSION) ($$(git rev-parse HEAD)).\n\
Committed on $$(git show -s HEAD --pretty=format:'%cd')." > dist/latexpand/version.txt
	git ls-files | \
		grep -v -e '\.gitignore' \
			-e '^tests/' \
			-e '^Makefile' \
			-e '^README\.md' \
			-e '^\.gitlab-ci\.yml' | \
		tar -cf - -T - | \
		(cd dist/latexpand/ && tar xf -)
	perl -pi -e "s/\@LATEXPAND_VERSION\@/$(GIT_VERSION)/" \
		dist/latexpand/latexpand dist/latexpand/README
	cd dist && zip -r latexpand.zip latexpand/
	@ls -l $(PWD)/dist/latexpand.zip
force:
